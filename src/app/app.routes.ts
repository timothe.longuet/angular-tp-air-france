import { Routes } from '@angular/router';
import { ViewAirFranceComponent } from './components/view-airfrance/view-airfrance.component';

export const routes: Routes = [
  {
    path: 'decollages', component: ViewAirFranceComponent, data: { type: 'decollages' }
  },
  {
    path: 'atterrissages', component: ViewAirFranceComponent, data: { type: 'atterrissages' }
  },
  {
    path: '**', redirectTo: 'decollages'
  }
];
