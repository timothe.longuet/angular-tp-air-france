import {Component, Input, OnChanges, Output} from '@angular/core';
import {Passager} from "../../models/passager.model";
import {MatIcon} from "@angular/material/icon";
import {AppComponent} from "../../app.component";
import {AppModule} from "../../app.module";
import {CommonModule, NgIf} from "@angular/common";
import {MatTooltip} from "@angular/material/tooltip";

@Component({
  selector: 'app-passager',
  standalone: true,
  imports: [
    MatIcon,
    AppComponent,
    AppModule,
    NgIf,
    CommonModule,
    MatTooltip,

  ],
  templateUrl: './passager.component.html',
  styleUrls: ['./passager.component.scss']
})
export class PassagerComponent implements OnChanges{
  @Input() passager: Passager | undefined;
  @Input() showPhotos = false;

  ngOnChanges() {
    console.log(this.passager)
  }
}
