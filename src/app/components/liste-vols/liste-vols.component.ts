import {Component, EventEmitter, Input, Output} from '@angular/core';
import {NgForOf} from "@angular/common";
import {Vol} from "../../models/vol.model";
import {VolComponent} from "../vol/vol.component";

@Component({
  selector: 'app-liste-vols',
  standalone: true,
  imports: [
    NgForOf,
    VolComponent
  ],
  templateUrl: './liste-vols.component.html',
  styleUrls: ['./liste-vols.component.scss']
})
export class ListeVolsComponent {
  @Input() vols: Vol[] = [];
  @Output() volSelected=  new EventEmitter<Vol>();
  getVolToPassager(vol: Vol | undefined): void {
     this.volSelected.emit(vol);
  }
}
