import {Component, Input} from '@angular/core';
import {Passager} from "../../models/passager.model";
import {CommonModule, NgForOf, NgIf} from "@angular/common";
import {VolComponent} from "../vol/vol.component";
import {PassagerComponent} from "../passager/passager.component";
import {MatSlideToggle} from "@angular/material/slide-toggle";
import {FormsModule} from "@angular/forms";

@Component({
  selector: 'app-liste-passagers',
  standalone: true,
  imports: [
    NgForOf,
    VolComponent,
    PassagerComponent,
    MatSlideToggle,
    FormsModule,
    NgIf,
    CommonModule,
  ],
  templateUrl: './liste-passagers.component.html',
  styleUrls: ['./liste-passagers.component.scss']
})
export class ListePassagersComponent {
  showPhotos = false;


  @Input() passegers: Passager[] | undefined;
  constructor() {}




}
