import {Component, EventEmitter, Output, OnInit, NgModule} from '@angular/core';
import { FiltresComponent } from '../filtres/filtres.component';
import { ListeVolsComponent } from '../liste-vols/liste-vols.component';
import { ListePassagersComponent } from '../liste-passagers/liste-passagers.component';
import {IAeroport} from "../../models/aeroport.model";
import {AEROPORTS} from "../../constants/aeroport.constant";
import {Vol} from "../../models/vol.model";
import {VolService} from "../../services/vol.service";
import {Passager} from "../../models/passager.model";
import { PassagerService } from '../../services/Passager.Service';
import {ActivatedRoute} from "@angular/router";
import {NgIf} from "@angular/common";
@Component({
  selector: 'app-view-airfrance',
  standalone: true,
  imports: [FiltresComponent, ListeVolsComponent, ListePassagersComponent, NgIf],
  templateUrl: './view-airfrance.component.html',
  styleUrls: ['./view-airfrance.component.scss']
})
export class ViewAirFranceComponent implements OnInit{
  startDate: Date | undefined;
  endDate: Date | undefined;
  aeroport: IAeroport | undefined;
  vols: Vol[] = [];
  vol: Vol | undefined;
  passagers: Passager[] = [];
  @Output() volListe = new EventEmitter<Vol[]>();
  @Output() volPassager = new EventEmitter<Passager[]>();
  type: string | undefined;
  constructor(private volService: VolService,
              private passagerService: PassagerService,
              private route: ActivatedRoute) {}

  handleFiltersApplied($event: { airport: IAeroport; startDate: Date; endDate: Date }) {
    this.aeroport = AEROPORTS.find(a => a.icao === $event.airport.icao);
    this.startDate = $event.startDate;
    this.endDate = $event.endDate;
    this.handleListeVols();
  }

  ngOnInit(): void {
    this.route.data.subscribe(data => {
      this.type = data['type'];
    });
  }
  handleListeVols(){
    if (!this.aeroport?.nom || !this.startDate || !this.endDate) {
      return;
    }
    if (this.type === 'decollages') {
      this.volService.getVolsDepart(this.aeroport.icao, this.startDate.getTime(), this.endDate.getTime())
        .subscribe(vols => {
          this.vols = vols;
          this.volListe.emit(this.vols);
        });
    }
    else if (this.type === 'atterrissages') {
      this.volService.getVolsArrivee(this.aeroport.icao, this.startDate.getTime(), this.endDate.getTime())
        .subscribe(vols => {
          this.vols = vols;
          this.volListe.emit(this.vols);
        });
    }
  }

  handleVolSelected(vol: Vol) {
    this.vol = vol;
    this.passagerService.getPassagers(vol.icao).subscribe(passagers => {
      this.passagers = passagers;
      this.volPassager.emit(this.passagers);
    });
  }
}
