import {Component, Input} from '@angular/core';
import {Vol} from "../../models/vol.model";
import {DatePipe, NgForOf, NgIf} from "@angular/common";
import {MatIcon} from "@angular/material/icon";

@Component({
  selector: 'app-vol',
  standalone: true,
  imports: [
    NgForOf,
    DatePipe,
    MatIcon,
    NgIf
  ],
  templateUrl: './vol.component.html',
  styleUrls: ['./vol.component.scss']
})
export class VolComponent {
  @Input() vol: Vol | undefined;



}
