import {Injectable} from "@angular/core";
import {map} from "rxjs";
import {IPassagerDto, Passager} from "../models/passager.model";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})
export class PassagerService {

  constructor(private http: HttpClient) { }

  getPassagers(iceao: string) {
    return this.http.get<{results: IPassagerDto[]}>(`https://randomuser.me/api?results=20&inc=name,email,picture&seed=${iceao}`).pipe(
      map((response) => response.results
        .map((dto: IPassagerDto) => new Passager(dto))
      ));
  }
}
