import { Directive, ElementRef, Input, OnChanges, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appClasseVol]',
  standalone: true,
})
export class ClasseVolDirective implements OnChanges {
  @Input('appClasseVol') classeVol: string = '';

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnChanges() {
    let color: string;
    switch (this.classeVol) {
      case 'STANDARD':
        color = 'blue';
        break;
      case 'BUSINESS':
        color = 'green';
        break;
      case 'PREMIUM':
        color = 'gold';
        break;
      default:
        color = 'black';
    }
    this.renderer.setStyle(this.el.nativeElement, 'color', color);
  }
}
