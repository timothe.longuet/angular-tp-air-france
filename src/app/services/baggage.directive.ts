import { Directive, ElementRef, Input, OnChanges, Renderer2 } from '@angular/core';

@Directive({
  selector: '[appBaggageRule]',
})
export class BaggageRuleDirective implements OnChanges {
  @Input('appBaggageRule') baggageCount: number = 0;
  @Input() ClassVol: string = '';

  constructor(private el: ElementRef, private renderer: Renderer2) {}

  ngOnChanges() {
    let limit: number;
    switch (this.ClassVol) {
      case 'STANDARD':
        limit = 1;
        break;
      case 'BUSINESS':
        limit = 2;
        break;
      case 'PREMIUM':
        limit = 3;
        break;
      default:
        limit = 1;
    }
    console.log('limit', limit);
    console.log('baggageCount', this.baggageCount);
    console.log('classVol', this.ClassVol);
    if (this.baggageCount > limit) {
      this.renderer.setStyle(this.el.nativeElement, 'background-color', 'red');
    } else {
      this.renderer.removeStyle(this.el.nativeElement, 'background-color');
    }
  }
}
