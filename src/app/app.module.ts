import {NgModule} from "@angular/core";
import {ClasseVolDirective} from "./services/classeVol.directive";
import {BaggageRuleDirective} from "./services/baggage.directive";
import {MatSlideToggleModule} from "@angular/material/slide-toggle";

@NgModule({
  imports: [
    ClasseVolDirective,
    MatSlideToggleModule,
  ],
  declarations: [
    BaggageRuleDirective
  ],
  exports: [
    ClasseVolDirective,
    BaggageRuleDirective
  ],
})
export class AppModule {
}
